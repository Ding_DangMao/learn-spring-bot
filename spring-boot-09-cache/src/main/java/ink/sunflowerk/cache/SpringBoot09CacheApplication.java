package ink.sunflowerk.cache;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

/**
 * @author zhaokuii11@163.com
 * @create 2022-01-05 10:15
 * @Description
 * 一、搭建基本文件
 * 1. 导入数据库文件 创建 department和 employee表
 * 2. 创建 JavaBean对象 实现  Serializable
 * 3. 整合 mybatis操作数据库
 * ==》3.1 配置数据源
 * ==》3.2 使用注解版的 mybatis
 * ====》a. 使用 @MapperScan 指定需要扫描 mapper接口所在的包
 * ====》b. 接口使用 @Mapper表明这是一个 mybatis的 mapper类
 * <p>
 * 二、快速体验缓存
 * 1. 开启基于注解的缓存
 * 2. 标注缓存注解即可
 * ==》@Cacheable
 * ==》@CacheEvict
 * ==》@CachePut
 * 默认使用的是 ConcurrentMapcacheManager=ConcurrentMapCache，将数据保存在 ConcurrentMap<Object,Object>
 * 开发中使用的是缓存中间件：redis，memcached，ehcache
 * 三、整合 Redis作为缓存
 * 》1、安装redis：使用docker；
 * 》2、引入redis的starter
 * 》3、配置redis
 * 》4、测试缓存
 * ====》原理：CacheManager===Cache 缓存组件来实际给缓存中存取数据
 * ====》1）、引入redis的starter，容器中保存的是 RedisCacheManager；
 * ====》2）、RedisCacheManager 帮我们创建 RedisCache 来作为缓存组件；RedisCache通过操作redis缓存数据的
 * ====》3）、默认保存数据 k-v 都是Object；利用序列化保存；如何保存为json
 * ========》1、引入了redis的starter，cacheManager变为 RedisCacheManager；
 * ========》2、默认创建的 RedisCacheManager 操作redis的时候使用的是 RedisTemplate<Object, Object>
 * ========》3、RedisTemplate<Object, Object> 是 默认使用jdk的序列化机制
 * ====》4）、自定义CacheManager；
 */
@MapperScan("ink.sunflowerk.cache.mapper")
@SpringBootApplication
@EnableCaching //开启基于注解的缓存
public class SpringBoot09CacheApplication {
    public static void main(String[] args) {
        SpringApplication.run(SpringBoot09CacheApplication.class, args);
    }
}
