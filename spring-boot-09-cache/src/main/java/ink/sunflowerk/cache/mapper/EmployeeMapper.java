package ink.sunflowerk.cache.mapper;

import ink.sunflowerk.cache.bean.Employee;
import org.apache.ibatis.annotations.*;

/**
 * @author zhaokuii11@163.com
 * @create 2022-01-05 10:30
 * @Description
 */
    @Mapper //显示的声明这是一个 mybatis的mapper类
    public interface EmployeeMapper {
        @Select("SELECT * FROM employee WHERE id = #{id}")
        public Employee getEmpById(Integer id);

        @Update("UPDATE employee SET lastName=#{lastName},email=#{email},gender=#{gender},d_id=#{dId} WHERE id=#{id}")
        public void updateEmp(Employee employee);

        @Delete("DELETE FROM employee WHERE id=#{id}")
        public void deleteEmpById(Integer id);

        @Insert("INSERT INTO employee(lastName,email,gender,d_id) VALUES(#{lastName},#{email},#{gender},#{dId})")
        public void insertEmployee(Employee employee);

        @Select("SELECT * FROM employee WHERE lastName = #{lastName}")
        Employee getEmpByLastName(String lastName);
    }
