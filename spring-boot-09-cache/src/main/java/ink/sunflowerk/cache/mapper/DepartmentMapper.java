package ink.sunflowerk.cache.mapper;

import ink.sunflowerk.cache.bean.Department;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

/**
 * @author zhaokuii11@163.com
 * @create 2022-01-05 10:31
 * @Description
 */
    @Mapper //显示的声明这是一个 mybatis的mapper类
    public interface DepartmentMapper {
        @Select("SELECT * FROM department WHERE id = #{id}")
        Department getDeptById(Integer id);
    }