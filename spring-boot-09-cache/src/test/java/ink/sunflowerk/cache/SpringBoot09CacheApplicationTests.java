package ink.sunflowerk.cache;

import ink.sunflowerk.cache.bean.Employee;
import ink.sunflowerk.cache.mapper.EmployeeMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

/**
 * @author zhaokuii11@163.com
 * @create 2022-01-05 10:37
 * @Description
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class SpringBoot09CacheApplicationTests {
    @Resource
    EmployeeMapper employeeMapper;
    @Autowired
    StringRedisTemplate stringRedisTemplate;//操作都是字符串的
    @Autowired
    RedisTemplate redisTemplate;//k-v 操作对象的
    @Autowired
    RedisTemplate<Object, Employee> empRedisTemplate;

    /**
     * Redis常用的五大数据类型
     * String字符串，List列表，Set集合，Hash散列，ZSet有序集合
     * stringRedisTemplate.opsForValue()
     */
    @Test
    public void testRedis() {
//        stringRedisTemplate.opsForValue().set("msg","hello" );
        String msg = stringRedisTemplate.opsForValue().get("msg");
        System.out.println(msg);
    }

    @Test
    public void testRedis2() {
        Employee emp = employeeMapper.getEmpById(1);
        System.out.println(emp);
        //默认如果保存对象，使用 jdk序列化机制，序列化后的数据保存到 redis中
        //redisTemplate.opsForValue().set("emp-01", emp);
        /*
         * 1. 将数据以 json的方式保存
         * =》a. 自己将对象转为 json
         * =》b. redisTemplate默认的序列化规则
         * */
//        empRedisTemplate.opsForValue().set("emp-02", emp);
        Employee employee = empRedisTemplate.opsForValue().get("emp-02");
        System.out.println(employee);
    }

    @Test
    public void contextLoads() {
        employeeMapper.insertEmployee(new Employee(1, "zhaoliu", "zhaoliu.@qq.com", 1, 1));
        System.out.println(employeeMapper.getEmpById(1));
    }
}
