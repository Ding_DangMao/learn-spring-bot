package com.cainiao.springboot.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author shkstart
 * @create 2021-09-23 23:09
 */
//@ResponseBody //这个类的所有方法返回的数据写给浏览器.(如果是 对象转为 json数据)
//@Controller
@RestController //RestController 就是 controller和 responseBody的合体
public class HelloController {

    @RequestMapping("/hello")
    public String hello() {
        return "hello world quick!";
    }
}
