idea都支持使用 Spring的项目创建向导快速创建一个 Spring Boot项目
选择我们生成的 Spring Boot项目
- 主程序已经生成好了,我们只需要编写字节的逻辑
- resource文件夹中目录结构
    
    static:保存静态资源;js,css,images
    
    templates:保存所有模板页面;(Spring Boot默认jar包嵌入式的 tomcat,默认不支持 jsp页面);可以使用模板引擎(freemarker,thymeleaf)
    
    application.properties: Spring Boot应用的配置文件;可以修改一些默认配置
    