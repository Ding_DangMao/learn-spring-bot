package ink.sunflowerk.ticket.service;

import com.alibaba.dubbo.config.annotation.Service;
import org.springframework.stereotype.Component;

/**
 * @author zhaokuii11@163.com
 * @create 2022-01-25 11:04
 * @Description
 */
@Component
@Service //将服务发部出去 com.alibaba.dubbo.config.annotation.Service;
public interface TicketService {
    public String getTicket();
}
