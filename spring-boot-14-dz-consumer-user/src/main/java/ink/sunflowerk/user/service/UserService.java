package ink.sunflowerk.user.service;

import com.alibaba.dubbo.config.annotation.Reference;
import ink.sunflowerk.ticket.service.TicketService;
import org.springframework.stereotype.Service;

/**
 * @author zhaokuii11@163.com
 * @create 2022-01-25 11:09
 * @Description
 */
@Service
public class UserService {
    @Reference
    TicketService ticketService;

    public void hello() {
        System.out.println(ticketService.getTicket());
        System.out.println("买到票了");
    }
}
