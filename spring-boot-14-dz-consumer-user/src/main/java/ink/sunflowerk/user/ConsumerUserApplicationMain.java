package ink.sunflowerk.user;

import com.alibaba.dubbo.config.spring.context.annotation.EnableDubbo;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author zhaokuii11@163.com
 * @create 2022-01-25 11:08
 * @Description 1. 引入依赖
 * 2. 配置 dubbo的注册中心地址
 * 3. 引用服务
 */
@EnableDubbo
@SpringBootApplication
public class ConsumerUserApplicationMain {
    public static void main(String[] args) {
        SpringApplication.run(ConsumerUserApplicationMain.class, args);
    }
}
