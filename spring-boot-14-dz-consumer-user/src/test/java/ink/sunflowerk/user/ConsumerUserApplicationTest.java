package ink.sunflowerk.user;

import ink.sunflowerk.user.service.UserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author zhaokuii11@163.com
 * @create 2022-01-25 11:32
 * @Description
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class ConsumerUserApplicationTest {
    @Autowired
    UserService userService;

    @Test
    public void testUserService() {
        //java.lang.reflect.InvocationTargetException: null 为解决
        userService.hello();
    }
}
