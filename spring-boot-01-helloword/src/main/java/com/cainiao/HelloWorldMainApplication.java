package com.cainiao;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author shkstart
 * @create 2021-09-23 20:08
 * @SpringBootApplication 来标注一个主程序类, 说明这是一个Spring Boot应用
 * Spring Boot 应用,标注在某个类上说明这个类是 Spring Boot的主配置类
 * spring boot就应该运行这个类的 main方法来启动 spring boot应用;
 *
 * SpringBootApplication 里面的
 * @Target({ElementType.TYPE})
 * @Retention(RetentionPolicy.RUNTIME)
 * @Documented
 * @Inherited
 * @SpringBootConfiguration :spring boot的配置类,表示这是一个 spring boot的配置类
 * ====>@Configuration Spring底层注解 配置类上标注这个注解-->配置文件;配置类也是容器中的一个组件; @component
 * @EnableAutoConfiguration 开启自动配置功能;以前我们需要配置的东西,spring boot帮我们自动配置
 * ====>@EnableAutoConfiguration告诉 Spring boot开启自动配置功能;这样自动配置才能生效;
 * ========>@AutoConfigurationPackage 自动配置包;将主配置类(@SpringBootApplication注解的类)的所在的包及下面所有子包里面的所有组件扫描到 Spring容器
 * ============>@Import{Registrar.class} spring底层注解@Import,给容器中导入一个组件;导入的组件由 Registrar.class决定
 * ========>@Import({EnableAutoConfigurationImportSelector.class (导入那些组件的选择器)}) 将所有需要导入的组件以全类名的方式返回;
 * ========>这些组件就会被添加到容器中;会给容器中导入非常多的自动配置类(xxxAutoConfiguration);就是给容器中导入这个场景的所有组件,并配置号这些组件;
 * ========>有了自动配置类,免去了我们手动编写配置注入功能组件等工作;
 * ============>SpringFactoriesLoader.loadFactoryNames(EnableAutoConfiguration.class,ClassLoader);spring boot在启动的时候从类路径下 META-INF/spring.factories中获取 EnableAutoConfiguration指定的值,将这些值作为自动配置类导入到容器中,自动配置类就会生效,帮我们进行自动配置工作;以前我们需要自己配置的东西,自动配置类都帮我们做了
 * @ComponentScan(
 *     excludeFilters = {@Filter(
 *     type = FilterType.CUSTOM,
 *     classes = {TypeExcludeFilter.class}
 * ), @Filter(
 *     type = FilterType.CUSTOM,
 *     classes = {AutoConfigurationExcludeFilter.class}
 * )}
 * )
 *
 * j2ee 的整体整合解决方案和自动配置都在 spring-boot-autoconfigure-1.5.9.RELEASE.jar
 */
@SpringBootApplication
public class HelloWorldMainApplication {
    public static void main(String[] args) {
        //Spring 应用启动起来
        SpringApplication.run(HelloWorldMainApplication.class, args);
    }
}
