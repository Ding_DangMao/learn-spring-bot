package ink.sunflowerk.task;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.test.context.junit4.SpringRunner;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.File;
import java.time.LocalDateTime;

/**
 * @author zhaokuii11@163.com
 * @create 2022-01-21 22:23
 * @Description
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class SpringApplicationTaskTest {
    @Autowired
    JavaMailSender mailSender;

    //发送普通邮件
    @Test
    public void testSendEmail() {
        SimpleMailMessage message = new SimpleMailMessage();
        //邮件设置
        message.setSubject(LocalDateTime.now().toString());//标题
        message.setText("i have girl for you");//内容
        message.setTo("2511862286@qq.com");//发给谁
        message.setFrom("2441972742@qq.com");//邮件是谁发的
        mailSender.send(message);
    }

    //发送附件的邮件
    @Test
    public void testSendEmail2() throws Exception {
        //1. 创建一个复杂的消息邮件
        MimeMessage mimeMessage = mailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true);
        //邮件设置
        helper.setSubject(LocalDateTime.now().toString());//标题
        helper.setText("<b style='color:pink'>i have girl for you</b>", true);//内容 true代表支持html
        helper.setTo("2511862286@qq.com");//发给谁
        helper.setFrom("2441972742@qq.com");//邮件是谁发的
        //上传文件
        String path = "C:\\Users\\KAlways18\\Downloads\\ing\\94850870_p0_master1200.jpg";
        FileSystemResource file = new FileSystemResource(new File(path));
        helper.addAttachment("美女图片.jpeg", file);
        mailSender.send(mimeMessage);
    }
}
