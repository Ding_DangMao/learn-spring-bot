package ink.sunflowerk.task.controller;

import ink.sunflowerk.task.service.AsyncService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author zhaokuii11@163.com
 * @create 2022-01-21 21:42
 * @Description
 */
@RestController
public class AsyncController {
    @Autowired
    AsyncService asyncService;

    @GetMapping("/hello")
    public String hello() {
        asyncService.hello();
        return "hello word";
    }
}
