package ink.sunflowerk.task.service;

import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

/**
 * @author zhaokuii11@163.com
 * @create 2022-01-21 21:40
 * @Description
 */
@Service
public class AsyncService {

    //告诉 spring这是一个异步方法，还需要在启动类上开启 @EnableAsync(开启异步注解)
    @Async
    public void hello() {
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("处理数据中");
    }
}
