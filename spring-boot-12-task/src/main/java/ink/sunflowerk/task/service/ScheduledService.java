package ink.sunflowerk.task.service;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

/**
 * @author zhaokuii11@163.com
 * @create 2022-01-21 21:51
 * @Description
 */

@Service
public class ScheduledService {
    /**
     * second,minute,hour,day of month,month,day fo week
     * 秒 分 时 日 月 周
     * 0/5 * * * * ? 每五秒执行一次
     */
    //@Scheduled(cron = "0/5 * * * *  ?")//每五秒执行一次
    //@Scheduled(cron = "0,1,2,4,5 * * * * MON-FRI")//周一到周五 1-5秒都会启动
    //@Scheduled(cron = "0-5 * * * * MON-FRI")//周一到周五 1-5秒都会启动
    @Scheduled(cron = "0/5 * * * * MON-FRI")//周一到周五 每5秒都会启动
    public void hello() {
        System.out.println("你好，我被执行了");
    }
}
