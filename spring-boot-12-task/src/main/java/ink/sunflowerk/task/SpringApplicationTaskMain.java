package ink.sunflowerk.task;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * @author zhaokuii11@163.com
 * @create 2022-01-21 21:39
 * @Description
 */
@EnableAsync //开启异步注解功能
@EnableScheduling //开启基于注解的定时任务
@SpringBootApplication
public class SpringApplicationTaskMain {
    public static void main(String[] args) {
        SpringApplication.run(SpringApplicationTaskMain.class, args);
    }
}
