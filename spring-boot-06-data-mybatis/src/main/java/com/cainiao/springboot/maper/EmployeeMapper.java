package com.cainiao.springboot.maper;

import com.cainiao.springboot.pojo.Employee;
import org.apache.ibatis.annotations.Mapper;
import org.mybatis.spring.annotation.MapperScan;

/**
 * @author zhaokuii11@163.com
 * @create 2021-11-13 20:57
 * @Description
 */
@Mapper//@mapper 或者 @MapperScan 将接口扫描装配到容器中
public interface EmployeeMapper {
    public Employee insertEmp(Employee employee);

    public Employee getEmpById(Integer id);
}
