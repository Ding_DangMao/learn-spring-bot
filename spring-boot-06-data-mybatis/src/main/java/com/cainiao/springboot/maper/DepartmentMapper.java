package com.cainiao.springboot.maper;

import com.cainiao.springboot.pojo.Department;
import org.apache.ibatis.annotations.*;

/**
 * @author zhaokuii11@163.com
 * @create 2021-11-13 20:07
 * @Description
 */
//这是一个操作数据库的 mapper
@Mapper
public interface DepartmentMapper {
    @Select("select * from department where id=#{id}")
    public Department getDeptById(Integer id);

    @Delete("delete from  department where id=#{id}")
    public int deleteDeptById(Integer id);

    //       是不是使用自动生成的主键    封装的主键的
    @Options(useGeneratedKeys = true, keyProperty = "id")
    @Insert("insert into department(departmentName) values(#{departmentName})")
    public int insertDept(Department department);

    @Update("update department set departmentName=#{departmentName} where id =#{id}")
    public int updateDept(Department department);
}
