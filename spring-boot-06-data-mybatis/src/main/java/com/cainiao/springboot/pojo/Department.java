package com.cainiao.springboot.pojo;

/**
 * @author zhaokuii11@163.com
 * @create 2021-11-13 20:02
 * @Description
 */
public class Department {
    private Integer id;
    private String departmentName;

    public String getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
