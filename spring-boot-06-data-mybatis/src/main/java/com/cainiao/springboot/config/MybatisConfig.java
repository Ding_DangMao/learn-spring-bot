package com.cainiao.springboot.config;

import org.apache.ibatis.session.Configuration;
import org.mybatis.spring.boot.autoconfigure.ConfigurationCustomizer;
import org.springframework.context.annotation.Bean;

/**
 * @author zhaokuii11@163.com
 * @create 2021-11-13 20:22
 * @Description
 */
//自定义 mybatis的全局设置 ==》驼峰命名 MybatisAutoConfiguration
public class MybatisConfig {
    @Bean
    public ConfigurationCustomizer configurationCustomizer() {

        //函数式编程
        return configuration -> configuration.setMapUnderscoreToCamelCase(true);
        /*return new ConfigurationCustomizer() {
            @Override
            public void customize(Configuration configuration) {
                configuration.setMapUnderscoreToCamelCase(true);
            }
        };*/
    }
}
