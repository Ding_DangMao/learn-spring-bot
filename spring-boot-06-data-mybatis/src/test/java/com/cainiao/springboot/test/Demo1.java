package com.cainiao.springboot.test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.sql.DataSource;
import java.sql.SQLException;

/**
 * @author zhaokuii11@163.com
 * @create 2021-11-13 19:33
 * @Description
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class Demo1 {

    @Autowired
    DataSource dataSource;

    @Test
    public void contextLoads() throws SQLException {

    }
}
