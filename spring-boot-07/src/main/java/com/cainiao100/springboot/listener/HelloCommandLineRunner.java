package com.cainiao100.springboot.listener;

import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.Arrays;

/**
 * @author zhaokuii11@163.com
 * @create 2021-11-14 13:58
 * @Description
 */
@Component
public class HelloCommandLineRunner implements CommandLineRunner {
    @Override
    public void run(String... args) throws Exception {
        System.out.println("HelloCommandLineRunner::" + Arrays.toString(args));
    }
}
