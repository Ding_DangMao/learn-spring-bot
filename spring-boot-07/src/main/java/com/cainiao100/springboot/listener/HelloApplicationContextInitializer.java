package com.cainiao100.springboot.listener;

import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;

/**
 * @author zhaokuii11@163.com
 * @create 2021-11-14 13:50
 * @Description
 */
public class HelloApplicationContextInitializer implements
        ApplicationContextInitializer<ConfigurableApplicationContext> {

    @Override
    public void initialize(ConfigurableApplicationContext configurableApplicationContext) {
        System.out.println("HelloApplicationContextInitializer方法运行了：：" + configurableApplicationContext);
    }
}
