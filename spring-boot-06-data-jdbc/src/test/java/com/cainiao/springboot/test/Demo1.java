package com.cainiao.springboot.test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * @author zhaokuii11@163.com
 * @create 2021-11-13 16:08
 * @Description
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class Demo1 {

    @Autowired
    DataSource dataSource;

    @Test
    public void contextLoads() throws SQLException {
        System.out.println(dataSource.getClass());
        Connection connection = dataSource.getConnection();
        System.out.println("connection = " + connection);
        /*
        * org.apache.tomcat.jdbc.pool.DataSource@552ed807{ConnectionPool[defaultAutoCommit=null; \
        *   defaultReadOnly=null; defaultTransactionIsolation=-1; defaultCatalog=null;
        *   driverClassName=com.mysql.jdbc.Driver; maxActive=100; maxIdle=100; minIdle=10;
        *   initialSize=10; maxWait=30000; testOnBorrow=true; testOnReturn=false;
        *   timeBetweenEvictionRunsMillis=5000; numTestsPerEvictionRun=0;
        *   minEvictableIdleTimeMillis=60000; testWhileIdle=false; testOnConnect=false;
        *    password=********; url=jdbc:mysql://localhost:3306/test; username=root;
        *   validationQuery=SELECT 1; validationQueryTimeout=-1; validatorClassName=null;
        *    validationInterval=3000; accessToUnderlyingConnectionAllowed=true; removeAbandoned=false;
        *   removeAbandonedTimeout=60; logAbandoned=false; connectionProperties=null; initSQL=null;
        *   jdbcInterceptors=null; jmxEnabled=true; fairQueue=true; useEquals=true;
        *   abandonWhenPercentageFull=0; maxAge=0; useLock=false; dataSource=null; dataSourceJNDI=null;
        *   suspectTimeout=0; alternateUsernameAllowed=false; commitOnReturn=false; rollbackOnReturn=false;
        *    useDisposableConnectionFacade=true; logValidationErrors=false; propagateInterruptState=false;
        *    ignoreExceptionOnPreLoad=false; useStatementFacade=true; }
        * connection = ProxyConnection[PooledConnection[com.mysql.jdbc.JDBC4Connection@771cbb1a]]
        * */
    }
}
