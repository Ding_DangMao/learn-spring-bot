package ink.sunflowerk.security.config;

import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

/**
 * @author zhaokuii11@163.com
 * @create 2022-01-21 23:18
 * @Description
 */
@EnableWebSecurity
public class MySecurityConfig extends WebSecurityConfigurerAdapter {
    @Override
    public void configure(HttpSecurity http) throws Exception {
        // 定制请求的授权规则
        //让所有人都可以访问 /这个请求(欢迎页)
        http.authorizeRequests().antMatchers("/").permitAll()
                // 访问 /level1/** 需要 VIP1的权限
                .antMatchers("/level1/**").hasRole("VIP1")
                .antMatchers("/level2/**").hasRole("VIP2")
                .antMatchers("/level3/**").hasRole("VIP3");

        //开启自动配置的登录功能，效果，如果没有登录，没有权限就会来到登录页面
        http.formLogin()
                .usernameParameter("user").passwordParameter("pwd") //指定登录表单的表单参数
                .loginPage("/userlogin");//指定登录的url
        // 1. /login来到登录页
        // 2. /login?error表示登录失败
        // 3. 更多配置在 HttpSecurity
        // 4. 默认post形式的 /login代表处理登录
        // 5. 一旦定制 loginPage，那么 loginPage的 post请求就是登录 get就是去登录

        //开启自动配置的注销功能
        http.logout() ////访问 /logout 表示用户注销，清空 session，注销成功返回 /login?logout
                .logoutSuccessUrl("/");//注销成功以后来到欢迎页面

        //开启记住我功能
        http.rememberMe().rememberMeParameter("remember");//会将 cookie存入浏览器14天生效
        //登录成功以后，将 cookie发给浏览器保存，以后访问页面带上这个cookie，只要通过检查就可以免登录，
        //点击注销会删除 cookie


    }

    //定义认证规则
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.inMemoryAuthentication()
                //添加用户 zhangsan 密码 1234 角色 VIP1 VIP2
                .withUser("zhangsan").password("1234").roles("VIP1", "VIP2")
                .and()
                .withUser("lisi").password("1234").roles("VIP2", "VIP3")
                .and()
                .withUser("wangwu").password("1234").roles("VIP1", "VIP3");
    }
}
