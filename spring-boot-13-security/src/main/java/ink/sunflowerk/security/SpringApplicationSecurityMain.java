package ink.sunflowerk.security;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author zhaokuii11@163.com
 * @create 2022-01-21 23:05
 * @Description
 * 1. 引入 SpringSecurity 模块
 * 2. 编写 Security配置类继承 WebSecurityConfigurerAdapter，使用注解 @EnableWebSecurity添加在类上
 */
@SpringBootApplication
public class SpringApplicationSecurityMain {
    public static void main(String[] args) {
        SpringApplication.run(SpringApplicationSecurityMain.class, args);
    }
}
