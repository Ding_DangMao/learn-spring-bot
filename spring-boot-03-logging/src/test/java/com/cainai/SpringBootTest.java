package com.cainai;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author shkstart
 * @create 2021-10-21 12:18
 */
@RunWith(SpringRunner.class)
@org.springframework.boot.test.context.SpringBootTest
public class SpringBootTest {
    //记录器，记录日志
    Logger logger = LoggerFactory.getLogger(getClass());

    @Test
    public void test1() {
        //日志的级别，由低到高 trance<debug<info<warn<error
        //可以调整 输出的日志级别。
        logger.trace("这是 trace日志");
        logger.debug("这是 debug日志");
        //spring boot默认给我们使用的是 info级别的【输出 info即一下的内容】
        //没有指定级别的就用spring boot默认规定的级别【root级别】
        logger.info("这是 info日志");
        logger.warn("这是 warn日志");
        logger.error("这是 error日志");
    }
}
