package com.cainiao.springboot.controller;

import com.cainiao.springboot.entity.User;
import com.cainiao.springboot.repository.UserRepository;
import com.jayway.jsonpath.Predicate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

/**
 * @author zhaokuii11@163.com
 * @create 2021-11-13 23:50
 * @Description
 */
@RestController
public class UserController {
    @Autowired
    UserRepository userRepository;


    @GetMapping("/user/{id}")
    public User getUser(@PathVariable("id") Integer id) {
        return userRepository.findUserById(id);
    }

}
