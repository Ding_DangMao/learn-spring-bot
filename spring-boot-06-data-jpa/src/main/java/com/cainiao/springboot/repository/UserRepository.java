package com.cainiao.springboot.repository;

import com.cainiao.springboot.entity.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.Repository;

import java.util.Optional;

/**
 * @author zhaokuii11@163.com
 * @create 2021-11-13 23:22
 * @Description
 */

//继承 extends Repository<那个实体类,主键类型>
public interface UserRepository extends Repository<User, Integer> {
    //方法名称必须要遵循驼峰式命名规则，findBy(关键字) + 属性名称(首字母大写) + 条件查询(首字母大写)
    User findUserById(Integer id);

}
