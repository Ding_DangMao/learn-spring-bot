package com.cainiao.springboot.entity;

/**
 * @author zhaokuii11@163.com
 * @create 2021-11-13 23:00
 * @Description
 */

import javax.persistence.*;
import java.io.Serializable;


//使用 JPA 注解配置映射关系
@Entity //告诉 jpa这是一个实体类【和数据表映射的类】
@Table(name = "tabl_user")//@Table 来指定和那个表的对应，省略默认表名就是 user
public class User implements Serializable {
    @GeneratedValue(strategy = GenerationType.IDENTITY) //主键自增
    @Id //这是一个主键
    private Integer id;
    @Column(name = "last_name")//这是和数据表对应的一个列
    private String lastName;
    @Column //省略 默认列明就是属性
    private String email;


    public Integer getId() {
        return id;
    }


    public void setId(Integer id) {
        this.id = id;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
