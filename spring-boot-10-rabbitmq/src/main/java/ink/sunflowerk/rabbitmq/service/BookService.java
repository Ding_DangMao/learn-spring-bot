package ink.sunflowerk.rabbitmq.service;

import ink.sunflowerk.rabbitmq.entity.Book;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Service;

/**
 * @author zhaokuii11@163.com
 * @create 2022-01-19 23:39
 * @Description 监听消息队列的内容
 */
@Service
public class BookService {

    //想要 rabbitMq的注解起作用，一定要开启基于注解模式
    @RabbitListener(queues = "atguigu.news")
    public void receive(Book book) {
        System.out.println("接收到消息:" + book);
    }

    @RabbitListener(queues = "atguigu")
    public void receive2(Message message) {
        System.out.println(message.getBody());
        System.out.println(message.getMessageProperties());
    }
}
