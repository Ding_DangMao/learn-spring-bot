package ink.sunflowerk.rabbitmq.config;

import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author zhaokuii11@163.com
 * @create 2022-01-19 23:13
 * @Description
 */
@Configuration
public class MyAMRPConfig {

    //org.springframework.amqp.support.converter.MessageConverter;
    @Bean
    public MessageConverter messageConverter() {
        return new Jackson2JsonMessageConverter();
    }
}
