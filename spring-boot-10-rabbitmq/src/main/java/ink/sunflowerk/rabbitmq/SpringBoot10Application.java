package ink.sunflowerk.rabbitmq;

import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author zhaokuii11@163.com
 * @create 2022-01-08 11:24
 * @Description 自动配置原理
 * 1. 自动配置类 org.springframework.boot.autoconfigure.amqp.RabbitAutoConfiguration
 * 2. 有自动配置了连接工厂 CachingConnectionFactory rabbitConnectionFactory(RabbitProperties config)
 * 3. RabbitProperties 封装了 rabbitmq的所有配置
 * 4. RabbitTemplate 给 rabbitmq发送和接收消息的
 * 5. AmqpAdmin rabbitmq系统的管理功能的组件
 * ==》AmqpAdmin：创建和删除 Queue，Exchange，Binding
 * 6. @EnableRabbit +  @RabbitListener 监听消息队列的内容
 */
@EnableRabbit //开启基于注解的 RabbitMq模式
@SpringBootApplication
public class SpringBoot10Application {
    public static void main(String[] args) {
        SpringApplication.run(SpringBoot10Application.class, args);
    }
}
