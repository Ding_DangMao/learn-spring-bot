package ink.sunflowerk.rabbitmq;

import ink.sunflowerk.rabbitmq.entity.Book;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.amqp.core.AmqpAdmin;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * @author zhaokuii11@163.com
 * @create 2022-01-08 11:24
 * @Description
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class SpringBoot10ApplicationTest {

    @Autowired
    RabbitTemplate rabbitTemplate;

    /**
     * 1. 单薄(点对点)
     */
    @Test
    public void testRabbitMqSend() {
        // Message 需要自己构造一个；定义消息内容和消息头
        // send(String exchange, String routingKey, Message message)

        // object 默认当成消息体，只需要传入要发送的对象，自动序列化发送给 rabbitmq
        // convertAndSend(String exchange, String routingKey, Object object)
       /* Map<String, Object> param = new HashMap<>();
        param.put("msg", "这是第一个消息");
        param.put("data", Arrays.asList("hello word", 123, false));*/

        // 对象被默认序列化以后发送出去
        /*
         * RabbitTemplate 类中可以看到
         * 默认使用的是 this.messageConverter = new SimpleMessageConverter();
         * */
        // 如何将数据自动的转为 json发送出去==>使用自定义的 MessageConverter
        rabbitTemplate.convertAndSend("exchange.direct", "atguigu.news",
                new Book("西游记", "吴承恩"));
    }

    //接收数据，
    @Test
    public void testReceive() {
        // 也可以使用
        // Message receive = rabbitTemplate.receive(String queueName);
        Object result = rabbitTemplate.receiveAndConvert("atguigu.news");
        System.out.println(result.getClass());// class java.util.HashMap
        System.out.println(result);// {msg=这是第一个消息, data=[hello word, 123, false]}
    }

    /**
     * 广播 fanout
     */
    @Test
    public void testBroadcastSend() {
        // 这里写错了，在创建的时候写错了，应该写成：exchange.fanout
        rabbitTemplate.convertAndSend("exchange.fancut", "", new Book("《进击的巨人》", "谏山创"));
    }

    @Autowired
    AmqpAdmin amqpAdmin;

    @Test
    public void createExchage() {
        // 创建交换器
//        amqpAdmin.declareExchange(new DirectExchange("amqpadmin.exchange"));
        // 创建队列
//        amqpAdmin.declareQueue(new Queue("amqpadmin.queue", true));
        // 创建绑定规则
        amqpAdmin.declareBinding(
                new Binding(
                        "amqpadmin.queue",
                        Binding.DestinationType.QUEUE,
                        "amqpadmin.exchange",
                        "amqp.haha",
                        null));
        // 也有删除，交换器，队列，绑定规则
    }

}
