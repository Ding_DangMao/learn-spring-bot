package com.cainiao100.springboot.bead;

import jdk.nashorn.internal.objects.annotations.Property;
import org.hibernate.validator.constraints.Email;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author shkstart
 * @create 2021-09-24 19:47
 * 将配文件中配置的每一个属性的值,映射到这个组件中
 *
 * @ConfigurationProperties:告诉 Spring Boot将本类中的所有属性和配置文件中相关的配置进项绑定
 * ====> prefix = "person" 配置文件中那个下面的所有属性进行--映射
 * 只有这个组件是容器中的组件,才能使用容器中提供 @ConfigurationProperties的功能
 * @ConfigurationProperties(prefix = "person") 默认从全局配置文件中获取值
 */
//@PropertySource(value = {"classpath:person.properties"})
//@Validated //JSR303数据校验 注入值的时候进行校验
@Component
@ConfigurationProperties(prefix = "person")
public class Person {
    /*
     * <bean>
     * <property name="lastName" value="字面量/${}从环境变量,配置文件中获取值/#{SpEL}"></property>
     * </bean>
     *
     * */
//    @Value("${person.last-name}")
//    @Email //lastName 必须是 邮箱格式
    private String lastName;
    //    @Value("#{11*2}")
    private Integer age;
    //    @Value("true")
    private Boolean boss;
    private Date birth;

    private Map<String, Object> map;
    private List<Object> list;
    private Dog dog;

    @Override
    public String toString() {
        return "Person{" +
                "lastName='" + lastName + '\'' +
                ", age=" + age +
                ", boss=" + boss +
                ", birth=" + birth +
                ", map=" + map +
                ", list=" + list +
                ", dog=" + dog +
                '}';
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Boolean getBoss() {
        return boss;
    }

    public void setBoss(Boolean boss) {
        this.boss = boss;
    }

    public Date getBirth() {
        return birth;
    }

    public void setBirth(Date birth) {
        this.birth = birth;
    }

    public Map<String, Object> getMap() {
        return map;
    }

    public void setMap(Map<String, Object> map) {
        this.map = map;
    }

    public List<Object> getList() {
        return list;
    }

    public void setList(List<Object> list) {
        this.list = list;
    }

    public Dog getDog() {
        return dog;
    }

    public void setDog(Dog dog) {
        this.dog = dog;
    }
}
