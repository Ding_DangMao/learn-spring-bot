package com.cainiao100.springboot.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author shkstart
 * @create 2021-09-25 15:20
 */
@RestController
public class HelloController {
    @Value("${person.last-name}")
    private String name;


    @RequestMapping("/hello")
    public String Hello() {
        return "hello "+name;
    }
}
