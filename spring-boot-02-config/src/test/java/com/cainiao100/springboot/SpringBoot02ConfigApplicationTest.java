package com.cainiao100.springboot;

import com.cainiao100.springboot.bead.Person;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author shkstart
 * @create 2021-09-24 20:05
 * spring boot 单元测试
 * <p>
 * 可以在测试期间很方便的类似编码一样进行自动注入等容器功能
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class SpringBoot02ConfigApplicationTest {
    @Autowired
    Person person;

    @Test
    public void contextLoads() {
        System.out.println(person);
        //Person{lastName='zhangsan', age=18, boss=false, birth=Sun Dec 17 00:00:00 CST 2017,
        // map={k1=v1, k2=12}, list=[lisi, zhaoliu], dog=Dog{name='小狗', age=2}}
    }

    @Autowired
    ApplicationContext ioc;

    @Test
    public void testHelloService() {
        System.out.println(ioc.containsBean("helloService"));
        //false
    }
}
