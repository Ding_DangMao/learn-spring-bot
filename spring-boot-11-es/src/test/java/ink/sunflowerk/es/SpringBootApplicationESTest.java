package ink.sunflowerk.es;

import ink.sunflowerk.es.entity.Article;
import ink.sunflowerk.es.entity.Book;
import ink.sunflowerk.es.repository.BookRepository;
import io.searchbox.client.JestClient;
import io.searchbox.core.Index;
import io.searchbox.core.Search;
import io.searchbox.core.SearchResult;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.io.IOException;

/**
 * @author zhaokuii11@163.com
 * @create 2022-01-21 9:36
 * @Description
 */
@SpringBootTest
public class SpringBootApplicationESTest {
    @Resource
    JestClient jestClient;

    @Test
    public void testES() {
        //1. 给es索引保存一个文档
        Article article = new Article(1, "zhangsan", "这是一个好消息", "hellowrod");
        // 构建一个索引功能
        Index index = new Index.Builder(article).index("atguigu").type("news").build();
        try {
            // 执行
            //浏览器访问 http://ip:9200/atguigu/news/1
            //或者 http://ip:9200/_search
            jestClient.execute(index);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testSearch() {
        //{query:{match:{last_name:hello}}}
        //查询表达式
        String json = "{" + "\"query\"" + ":" + "{" + "\"match\"" + ":" + "{" + "\"author\"" + ":" + "\"zhangsan\"" + "}" + "}" + "}";
        //构建搜索功能
        Search build = new Search.Builder(json).addIndex("atguigu").addType("news").build();
        try {
            SearchResult result = jestClient.execute(build);
            System.out.println(result.getJsonString());
            //{"took":3,"timed_out":false,"_shards":{
            // "total":1,"successful":1,"skipped":0,"failed":0},
            // "hits":{"total":{"value":1,"relation":"eq"},"max_score":0.2876821,
            // "hits":[{"_index":"atguigu","_type":"news","_id":"1","_score":0.2876821,
            // "_source":{"id":1,"author":"zhangsan","title":"这是一个好消息","content":"hellowrod"}}]}}
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Autowired
    BookRepository bookRepository;

    @Test
    public void testDataES() {
        //浏览器访问 ip:9200/atguigu/boot/1
        // 官方文档
        // https://docs.spring.io/spring-data/elasticsearch/docs/current/reference/html/#repositories.query-methods
        bookRepository.index(new Book(1, "大话西游", "jack"));
    }
}
