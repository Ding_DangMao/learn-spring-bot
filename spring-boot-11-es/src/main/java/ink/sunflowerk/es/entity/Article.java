package ink.sunflowerk.es.entity;

import io.searchbox.annotations.JestId;

import java.io.Serializable;

/**
 * @author zhaokuii11@163.com
 * @create 2022-01-21 10:15
 * @Description
 */
public class Article implements Serializable {
    @JestId //标识这是一个 主键
    private Integer id;
    private String author;
    private String title;
    private String content;

    public Article() {
    }

    @Override
    public String toString() {
        return "Article{" +
                "id=" + id +
                ", author='" + author + '\'' +
                ", title='" + title + '\'' +
                ", content='" + content + '\'' +
                '}';
    }

    public Article(Integer id, String author, String title, String content) {
        this.id = id;
        this.author = author;
        this.title = title;
        this.content = content;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
