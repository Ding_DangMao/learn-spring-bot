package ink.sunflowerk.es.repository;

import ink.sunflowerk.es.entity.Book;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * @author zhaokuii11@163.com
 * @create 2022-01-21 11:18
 * @Description
 */

public interface BookRepository extends ElasticsearchRepository<Book, Integer> {
}
