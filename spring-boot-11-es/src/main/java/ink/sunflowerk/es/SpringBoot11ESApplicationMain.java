package ink.sunflowerk.es;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author zhaokuii11@163.com
 * @create 2022-01-21 9:37
 * @Description SpringBoot默认支持两种技术和 ES交互
 * 1. Jest(org.springframework.boot.autoconfigure.elasticsearch.jest
 * ==>默认是不生效了，需要导入 jest的工具包 io.searchbox.client.JestClient;)
 * 2. SpringData ElasticSearch(org.springframework.boot.autoconfigure.data.elasticsearch)
 * ==> es版本有可能不合适，如果版本不合适：1. 升级 springboot版本，2. 安装对应的 es
 * ==>2.1 Client 节点信息 clusterNodes;clusterName
 * ==>2.2 ElasticsearchTemplate 操作 es
 * ==>2.3 编写一个 ElasticsearchRepository 的子接口来操作ES
 *
 */
@SpringBootApplication
public class SpringBoot11ESApplicationMain {
    public static void main(String[] args) {
        SpringApplication.run(SpringBoot11ESApplicationMain.class, args);
    }
}
