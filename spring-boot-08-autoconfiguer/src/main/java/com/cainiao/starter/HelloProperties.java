package com.cainiao.starter;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author zhaokuii11@163.com
 * @create 2021-11-14 19:04
 * @Description
 */
@ConfigurationProperties(prefix = "sunflower.hello")
public class HelloProperties {
    private String prefix;
    private String suffix;

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public String getSuffix() {
        return suffix;
    }

    public void setSuffix(String suffix) {
        this.suffix = suffix;
    }
}
