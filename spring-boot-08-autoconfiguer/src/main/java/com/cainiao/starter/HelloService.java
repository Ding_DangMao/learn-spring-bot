package com.cainiao.starter;

import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author zhaokuii11@163.com
 * @create 2021-11-14 19:03
 * @Description
 */
public class HelloService {

    private HelloProperties helloProperties;

    public String sayHello(String name){
            return helloProperties.getPrefix() + name + helloProperties.getSuffix();
    }

    public HelloProperties getHelloProperties() {
        return helloProperties;
    }

    public void setHelloProperties(HelloProperties helloProperties) {
        this.helloProperties = helloProperties;
    }
}
