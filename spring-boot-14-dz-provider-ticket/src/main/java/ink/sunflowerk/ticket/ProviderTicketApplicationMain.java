package ink.sunflowerk.ticket;

import com.alibaba.dubbo.config.spring.context.annotation.DubboComponentScan;
import com.alibaba.dubbo.config.spring.context.annotation.EnableDubbo;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author zhaokuii11@163.com
 * @create 2022-01-25 11:01
 * @Description 1. 将服务提供者注册到注册中心
 * ==》1. 引入 dubbo和zkclient相关依赖
 * ==》2. 配置 dubbo的扫描包和注册中心地址
 * ==》3. 使用 @Service发布服务
 */
@SpringBootApplication
@EnableDubbo
@DubboComponentScan(basePackages = "ink.sunflowerk.ticket.service")
public class ProviderTicketApplicationMain {
    public static void main(String[] args) {
        SpringApplication.run(ProviderTicketApplicationMain.class, args);
    }
}
