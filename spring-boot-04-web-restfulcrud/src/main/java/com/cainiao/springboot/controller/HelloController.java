package com.cainiao.springboot.controller;

import com.cainiao.springboot.exception.UserNotExistException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Arrays;
import java.util.Map;

/**
 * @author shkstart
 * @create 2021-10-24 22:01
 */
@Controller //代表当前类是一个控制器
public class HelloController {

   /* @RequestMapping({"/", "/index.html"})
    public String index() {
        return "index";
    }*/

    @ResponseBody //hello 发送出去需要依赖 responseBody
    @RequestMapping("/hello") //处理请求
    public String Hello(@RequestParam("user") String user) {
        if ("aaa".equals(user)) {
            throw new UserNotExistException();
        }
        return "hello world";
    }

    /**
     * 查出一些数据，在页面展示
     *
     * @return
     */
    @RequestMapping("/success")
    public String success(Map<String, Object> map) {
        // classpath:/templates/
        map.put("hello", "<h1>你好</h1>");
        map.put("users", Arrays.asList("张三", "李四", "王五"));
        return "success";
    }
}
