package com.cainiao.springboot.controller;

import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.Map;

/**
 * @author shkstart
 * @create 2021-10-27 12:46
 */
@Controller
public class LoginController {


    //    @RequestMapping(value="/user/login",method = RequestMethod.POST)
    //    @GetMapping
    @PostMapping("/user/login")
    public String login(@RequestParam("username") String username,
                        @RequestParam("password") String password,
                        Map<String, Object> map, HttpSession session) {

        System.out.println("username = " + username);
        System.out.println("password = " + password);
        if (!StringUtils.isEmpty(username) && "123456".equals(password)) {

            session.setAttribute("loginUser", username);
            //登录成功,防止表单重复提交，可以重定向到 主页
            return "redirect:/main.html";
        } else {
            //登录失败
            map.put("msg", "用户名密码错误");
            return "login";
        }
    }

    @GetMapping("/user/logout")
    public String logout(HttpSession session, Map<String, Object> map) {
        map.put("msg", "已成功退出登录");
        session.invalidate();
        return "login";
    }
}
