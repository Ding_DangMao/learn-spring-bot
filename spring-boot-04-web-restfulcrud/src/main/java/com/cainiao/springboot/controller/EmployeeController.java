package com.cainiao.springboot.controller;

import com.cainiao.springboot.dao.DepartmentDao;
import com.cainiao.springboot.dao.EmployeeDao;
import com.cainiao.springboot.entities.Department;
import com.cainiao.springboot.entities.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.Ordered;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.view.AbstractCachingViewResolver;
import org.thymeleaf.spring4.view.ThymeleafViewResolver;

import java.util.Collection;

/**
 * @author shkstart
 * @create 2021-10-28 22:17
 */
@Controller
public class EmployeeController {
    @Autowired
    private EmployeeDao employeeDao;
    @Autowired
    DepartmentDao departmentDao;

    //查询所有员工列表页面
    @GetMapping("/emps")
    public String list(Model model) {
        Collection<Employee> employees = employeeDao.getAll();
        //放在请求域中
        model.addAttribute("emps", employees);

        //thymeleaf 默认就会拼串
        //"classpath:/templates/"  ".html";
        return "emp/list";
    }

    //来到员工添加页面
    @GetMapping("/emp")
    public String toAddPage(Model model) {
        System.out.println("getmapping");
        //来到 添加页面,查出所有的部门，在页面显示
        Collection<Department> departments = departmentDao.getDepartments();
        model.addAttribute("depts", departments);
        return "emp/add";
    }

    //员工添加
    //springmvc 自动将请求参数和入参对象的属性镜像【绑定】，要求请求参数的名字和 JavaBean入参的对象里面的属性姓名是一样的
    @PostMapping("/emp")
    public String addEmp(Employee employee) {

        //来到员工列表页面
        //redirect 表示重定向到一个地址，forward 表示转发到一个地址 /代表当前路径
        /*
         * 原理：ThymeleafViewResolver extends AbstractCachingViewResolver implements Ordered
         * 该类属性
         * String REDIRECT_URL_PREFIX = "redirect:";
         * String FORWARD_URL_PREFIX = "forward:";
         * 该类方法
         * protected View createView(final String viewName, final Locale locale) throws Exception {
         *  RedirectView view = new RedirectView
         * }
         *
         * RedirectView类的该方法
         * protected void renderMergedOutputModel(Map<String, Object> model, HttpServletRequest request,
         *	HttpServletResponse response) throws IOException {
         *  sendRedirect(request, response, targetUrl, this.http10Compatible);
         * }
         *
         * protected void sendRedirect(HttpServletRequest request, HttpServletResponse response,
         *	String targetUrl, boolean http10Compatible) throws IOException {
         *  response.sendRedirect(encodedURL); //重定向
         * */
        System.out.println("add employee = " + employee);
        employeeDao.save(employee);//保存员工数据

        return "redirect:/emps";
    }

    //来到修改页面，查出当前员工，在页面回显
    @GetMapping("/emp/{id}")
    public String toEditPage(@PathVariable("id") Integer id, Model model) {
        Employee employee = employeeDao.get(id);
        model.addAttribute("emp", employee);
        //回到修改页面
        //页面要显示员工列表
        Collection<Department> departments = departmentDao.getDepartments();
        model.addAttribute("depts", departments);
        return "emp/add";
    }

    //员工修改 需要提交员工 ip
    @PutMapping("/emp")
    public String updateEmployee(Employee employee) {
        System.out.println("update employee = " + employee);
        employeeDao.save(employee);
        return "redirect:/emps";
    }

    //员工删除
    @DeleteMapping("/emp/{id}")
    public String deleteEmployee(@PathVariable("id") Integer id) {
        employeeDao.delete(id);
        return "redirect:/emps";
    }
}
