package com.cainiao.springboot.component;

import org.springframework.boot.autoconfigure.web.DefaultErrorAttributes;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestAttributes;

import java.util.Map;

/**
 * @author shkstart
 * @create 2021-10-31 17:21
 * 给容器中加入我们字节的ErrorAttributes
 */
@Component
public class MyErrorAttributes extends DefaultErrorAttributes {
    //返回值的map就是页面和 json能获取的所有字段
    @Override
    public Map<String, Object> getErrorAttributes(RequestAttributes requestAttributes, boolean includeStackTrace) {
        Map<String, Object> map = super.getErrorAttributes(requestAttributes, includeStackTrace);
        //给容器中添加自定义的数据：公司表示
        map.put("company", "cainiao");

        //从请求域中获取异常处理器携带的数据
        //第二个参数意思：int SCOPE_REQUEST = 0; int SCOPE_SESSION = 1; int SCOPE_GLOBAL_SESSION = 2;
        Map<String, Object> ext = (Map<String, Object>) requestAttributes.getAttribute("ext", 0);
        map.put("ext", ext);
        return map;
    }
}
