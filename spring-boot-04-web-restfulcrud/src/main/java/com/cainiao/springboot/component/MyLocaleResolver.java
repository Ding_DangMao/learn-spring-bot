package com.cainiao.springboot.component;

import org.springframework.util.StringUtils;
import org.springframework.web.servlet.LocaleResolver;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Locale;

/**
 * @author shkstart
 * @create 2021-10-26 23:23
 * 可以在连接上携带区域信息
 */
public class MyLocaleResolver implements LocaleResolver {

    //解析区域信息的
    @Override
    public Locale resolveLocale(HttpServletRequest request) {
        String l = request.getParameter("l");
        //获取到就根据获取到的，没有，默认的
        Locale locale = Locale.getDefault();
        if (!StringUtils.isEmpty(l)) {
            String[] split = l.split("_");
            //第一参数，语言代码，第二个，国家代码
            locale = new Locale(split[0], split[1]);
            System.out.println("使用自定义的 local");
        }
        return locale;
    }

    @Override
    public void setLocale(HttpServletRequest request, HttpServletResponse response, Locale locale) {

    }
}
