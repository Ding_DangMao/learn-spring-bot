package com.cainiao.springboot.config;

import com.cainiao.springboot.component.LoginHandlerInterceptor;
import com.cainiao.springboot.component.MyLocaleResolver;

import com.cainiao.springboot.filter.MyFilter;
import com.cainiao.springboot.listener.MyListener;
import com.cainiao.springboot.servlet.MyServlet;
import org.springframework.boot.context.embedded.ConfigurableEmbeddedServletContainer;
import org.springframework.boot.context.embedded.EmbeddedServletContainerCustomizer;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.servlet.ServletListenerRegistrationBean;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.LocaleResolver;

import java.util.Arrays;

/**
 * @author shkstart
 * @create 2021-10-26 19:31
 * 使用 WebMvcConfigurerAdapter可以来扩展 springmvc的功能
 */
//@EnableWebMvc
@Configuration
public class MyMvcConfig extends WebMvcConfigurerAdapter {

    /*
     * 注册三大组件
     * */
    @Bean
    public ServletRegistrationBean myServlet() {
        ServletRegistrationBean registrationBean =
                new ServletRegistrationBean(new MyServlet(), "/myServlet");
        registrationBean.setLoadOnStartup(1);
        return registrationBean;
    }

    @Bean
    public FilterRegistrationBean myFilter() {
        FilterRegistrationBean registrationBean = new FilterRegistrationBean();
        registrationBean.setFilter(new MyFilter());
        registrationBean.setUrlPatterns(Arrays.asList("/hello", "/myServlet"));
        return registrationBean;
    }

    @Bean
    public ServletListenerRegistrationBean myListener() {
        return new ServletListenerRegistrationBean<MyListener>(new MyListener());
    }


    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
//        super.addViewControllers(registry);
        //浏览器发送请求 /cainiao 来到 success页面
        registry.addViewController("/cainiao").setViewName("success");
    }

    //所有的 WebMvcConfigurerAdapter组件会一起起作用
    @Bean //将组建注册在容器中
    public WebMvcConfigurerAdapter getWebMvcConfigurerAdapter() {
        return new WebMvcConfigurerAdapter() {
            //视图控制器
            @Override
            public void addViewControllers(ViewControllerRegistry registry) {
                registry.addViewController("/").setViewName("login");
                registry.addViewController("/index.html").setViewName("login");
                registry.addViewController("/main.html").setViewName("dashboard");
            }

            //注册拦截器的
            @Override
            public void addInterceptors(InterceptorRegistry registry) {
                //静态资源： *.css *.js [spring boot已经做好了静态资源映射]
//                registry.addInterceptor(new LoginHandlerInterceptor())
//                        // /** 拦截任意目录下的所有请求
//                        .addPathPatterns("/**").excludePathPatterns("/index.html", "/user/login");
            }
        };
    }


    //名字 localeResolver 指明一下 否则会使用spring boot默认的
    @Bean(name = "localeResolver")
    public LocaleResolver localResolver() {
        return new MyLocaleResolver();
    }

    //配置嵌入式的 servlet容器
    @Bean
    public EmbeddedServletContainerCustomizer embeddedServletContainerCustomizer() {
        //定制嵌入式的 servlet容器相关的规则
        return new EmbeddedServletContainerCustomizer() {
            @Override
            public void customize(ConfigurableEmbeddedServletContainer container)  {
                container.setPort(8080);
            }
        };
    }
}
