package com.cainiao.springboot.exception;

/**
 * @author shkstart
 * @create 2021-10-30 21:54
 */
public class UserNotExistException extends RuntimeException {
    public UserNotExistException() {
        super("用户不存在");
    }
}
