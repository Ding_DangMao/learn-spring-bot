package com.cainiao;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @SpringBootApplication 失效
 * 注解失效的情况下，推荐使用@ComponentScan(”指定扫描路径“) 和@EnableAutoConfiguration进行代替；
 *
 * 【Spring Boot Application in default package 】嘛问题解决、大致是我放到 java下了
 */
//@ComponentScan("com.cainiao")
//@EnableAutoConfiguration
@SpringBootApplication
public class SpringBootMain {
    public static void main(String[] args) {
        SpringApplication.run(SpringBootMain.class, args);
    }
}
