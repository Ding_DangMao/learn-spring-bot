package com.cainiao;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @author zhaokuii11@163.com
 * @create 2021-11-04 22:20
 * @Description
 */
@Controller
public class HelloController {
    @GetMapping("/abc")
    public String hello(Model model) {
        model.addAttribute("msage", "你好啊");
        return "success";
    }
}
